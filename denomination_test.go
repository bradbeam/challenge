package main

import "testing"

var sampleInput Denominations = Denominations{
	Starting: 19239.39,
	Dollars:  19239,
	Cents:    39,
}
var testDollarResults map[int]int = map[int]int{
	hundredValue: 192,
	fiftyValue:   0,
	twentyValue:  1,
	tenValue:     1,
	fiveValue:    1,
	oneValue:     4,
}
var testCentsResults map[int]int = map[int]int{
	quarterValue: 1,
	dimeValue:    1,
	nickelValue:  0,
	pennyValue:   4,
}

func TestInput(t *testing.T) {
	// Need to wait until the validation gets split out from main

}

func TestHundredDenomination(t *testing.T) {
	sampleInput.updateDollars(hundredValue, false)
	if sampleInput.Number != testDollarResults[hundredValue] {
		t.Error("Did not get", testDollarResults[hundredValue], "$100 bills back, received", sampleInput.Number)
	}
}

func TestFiftyDenomination(t *testing.T) {
	sampleInput.updateDollars(fiftyValue, false)
	if sampleInput.Number != testDollarResults[fiftyValue] {
		t.Error("Did not get", testDollarResults[fiftyValue], "$50 bills back, received", sampleInput.Number)
	}
}

func TestTwentyDenomination(t *testing.T) {
	sampleInput.updateDollars(twentyValue, false)
	if sampleInput.Number != testDollarResults[twentyValue] {
		t.Error("Did not get", testDollarResults[twentyValue], "$20 bills back, received", sampleInput.Number)
	}
}

func TestTenDenomination(t *testing.T) {
	sampleInput.updateDollars(tenValue, false)
	if sampleInput.Number != testDollarResults[tenValue] {
		t.Error("Did not get", testDollarResults[tenValue], "$10 bills back, received", sampleInput.Number)
	}
}

func TestFiveDenomination(t *testing.T) {
	sampleInput.updateDollars(fiveValue, false)
	if sampleInput.Number != testDollarResults[fiveValue] {
		t.Error("Did not get", testDollarResults[fiveValue], "$5 bills back, received", sampleInput.Number)
	}
}

func TestOneDenomination(t *testing.T) {
	sampleInput.updateDollars(oneValue, false)
	if sampleInput.Number != testDollarResults[oneValue] {
		t.Error("Did not get", testDollarResults[oneValue], "$1 bills back, received", sampleInput.Number)
	}
}

func TestQuarterDenomination(t *testing.T) {
	sampleInput.updateCents(quarterValue, false)
	if sampleInput.Number != testCentsResults[quarterValue] {
		t.Error("Did not get", testCentsResults[quarterValue], "quarters back, received", sampleInput.Number)
	}
}

func TestDimeDenomination(t *testing.T) {
	sampleInput.updateCents(dimeValue, false)
	if sampleInput.Number != testCentsResults[dimeValue] {
		t.Error("Did not get", testCentsResults[dimeValue], "dimes back, received", sampleInput.Number)
	}
}

func TestNickelDenomination(t *testing.T) {
	sampleInput.updateCents(nickelValue, false)
	if sampleInput.Number != testCentsResults[nickelValue] {
		t.Error("Did not get", testCentsResults[nickelValue], "nickels back, received", sampleInput.Number)
	}
}

func TestPennyDenomination(t *testing.T) {
	sampleInput.updateCents(pennyValue, false)
	if sampleInput.Number != testCentsResults[pennyValue] {
		t.Error("Did not get", testCentsResults[pennyValue], "pennies back, received", sampleInput.Number)
	}
}
