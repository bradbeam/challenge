package main

import (
	"log"
	"os"
	"regexp"
	"strconv"
	"strings"
)

const hundredValue int = 100
const fiftyValue int = 50
const twentyValue int = 20
const tenValue int = 10
const fiveValue int = 5
const oneValue int = 1
const quarterValue int = 25
const dimeValue int = 10
const nickelValue int = 5
const pennyValue int = 1

type Denominations struct {
	Starting float64
	Dollars  int
	Cents    int
	Number   int
}

func (d *Denominations) updateDollars(value int, display bool) {
	d.Number = d.Dollars / value
	d.Dollars -= d.Number * value
	if display == true {
		log.Printf("%s%d %s %d\n", "$", value, "bills:", d.Number)
	}

}

func (d *Denominations) updateCents(value int, display bool) {
	d.Number = d.Cents / value
	d.Cents -= d.Number * value
	if display == true {
		log.Printf("%s%d %s %d\n", "", value, "cents:", d.Number)
	}
}

func main() {
	// Make sure we have appropriate length of input
	if len(os.Args) != 2 {
		log.Fatal("Invalid input.")
	}

	// Regex for d+.cc
	regex, err := regexp.Compile(`^\d+(\.\d{2})?$`)
	matched := regex.Match([]byte(os.Args[1]))

	if err != nil || matched == false {
		log.Fatal("Invalid input, must be in the form of dollars or dollars ( 12 ) and cents ( 11.04 )")
	}

	// Make sure input is a number
	currency := new(Denominations)

	// Save off starting value
	currency.Starting, err = strconv.ParseFloat(os.Args[1], 64)
	if err != nil {
		log.Fatal("Input is not a number")
	}

	// Get our dolla dolla bills
	parts := strings.Split(os.Args[1], ".")
	currency.Dollars, err = strconv.Atoi(parts[0])
	if err != nil {
		log.Fatal("Input is not a number")
	}

	if len(parts) == 2 {
		// And dont forget about the cents
		currency.Cents, err = strconv.Atoi(parts[1])
		if err != nil {
			log.Fatal("Input is not a number")
		}
	}

	log.Println("Starting currency: ", currency.Starting)
	currency.updateDollars(hundredValue, true)
	currency.updateDollars(fiftyValue, true)
	currency.updateDollars(twentyValue, true)
	currency.updateDollars(tenValue, true)
	currency.updateDollars(fiveValue, true)
	currency.updateDollars(oneValue, true)

	currency.updateCents(quarterValue, true)
	currency.updateCents(dimeValue, true)
	currency.updateCents(nickelValue, true)
	currency.updateCents(pennyValue, true)
}
